const compareEndpoint = ({ body: payload }, res, next) => {
    const fields = payload.Fields.map(field => field.Field);

    // Dimensions
    if (fields.includes('publisherId')) {
        res.locals.mockFile = './mocks/compare/compare_dimensions_publisher.json';
    } else if (fields.includes('seatId')) {
        res.locals.mockFile = './mocks/compare/compare_dimensions_seat.json';
    } else if (fields.includes('dealId')) {
        res.locals.mockFile = './mocks/compare/compare_dimensions_deal.json';
    } else if (fields.includes('appOrSiteDomain')) {
        res.locals.mockFile = './mocks/compare/compare_dimensions_appsitedomain.json';
    }

    // Metrics
    else if (fields.includes('hour') || fields.includes('day')) {
        res.locals.mockFile = './mocks/compare/compare_metrics_data.json';
    } else if (fields.includes('impressions') && fields.includes('smartGrossRevenueEuro')) {
        res.locals.mockFile = './mocks/compare/compare_metrics_total.json';
    } 

    else {
        res.status(422);
    }

    next();
};

module.exports = compareEndpoint;
