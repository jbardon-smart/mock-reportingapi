const fs = require('fs');

const reportsEndpoint = ({ body: payload }, res, next) => {
    if (payload.FileFormat === 'CSV') {
      res.setHeader('Content-Type', 'text/csv');

      // Download from Mock with file name
      // res.setHeader('Content-Disposition', 'attachment; filename = MonRapport.csv;');

      // Generate report with random data (ex: set to 1Gb with bs option)
      // dd if=/dev/zero of=report.csv count=1024 bs=1000000
      fs.createReadStream('./mocks/reports/reports.csv').pipe(res);
      return;
    }

    const fields = payload.Fields.map(field => field.Field);

    // Dimensions
    if (fields.includes('publisherId')) {
        res.locals.mockFile = './mocks/reports/reports_dimensions_publisher.json';
    } else if (fields.includes('seatId')) {
        res.locals.mockFile = './mocks/reports/reports_dimensions_seat.json';
    } else if (fields.includes('dealId')) {
        res.locals.mockFile = './mocks/reports/reports_dimensions_deal.json';
    } else if (fields.includes('appOrSiteDomain')) {
        res.locals.mockFile = './mocks/reports/reports_dimensions_appsitedomain.json';
    }

    // Metrics
    else if (fields.includes('hour') || fields.includes('day')) {
        res.locals.mockFile = './mocks/reports/reports_metrics_data.json';
    } else if (fields.includes('impressions') && fields.includes('smartGrossRevenueEuro')) {
        res.locals.mockFile = './mocks/reports/reports_metrics_total.json';
    }

    else {
        res.status(422);
    }

    next();
};

module.exports = reportsEndpoint;
