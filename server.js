const fs = require('fs');
const path = require('path');
const express = require('express');

const reports = require('./endpoints/reports');
const compare = require('./endpoints/compare');

const app = express();

app.use(express.json());

app.post('/reports', reports);
app.post('/compare', compare);

app.use(function ReturnMockFile(req, res, next) {
  const { mockFile } = res.locals;

  if (mockFile) {
    const mockFilePath = path.resolve(mockFile);
    const payload = JSON.parse(fs.readFileSync(mockFilePath));

    res.header('X-Mock-File', mockFile);
    res.json(payload);

    console.log(`HTTP POST ${req.path} responded ${res.statusCode} with file ${mockFile}`);
    console.log(JSON.stringify(req.body, null, 4));
  } else if (res.statusCode !== 200) {
    const fields = req.body.Fields.map(field => field.Field);
    const errorMessage = `No file for fields: ${fields}`;

    res.send(errorMessage);
    console.error(`HTTP POST ${req.path} responded ${res.statusCode} with error "${errorMessage}"`);
    console.log(JSON.stringify(req.body, null, 4));
  }

  console.log();
  next();
});

console.log('Reporting API is running on localhost:3011');
app.listen(3011);
